
#include <gst/gst.h>
#include <gst/webrtc/webrtc.h>
#include <json/json.h>

#include "cpp-httplib/httplib.h"

#define RTP_CAPS_VP8 "application/x-rtp,media=video,encoding-name=VP8,payload=96"

static void on_negotiation_needed(GstElement *element, gpointer user_data);
static void
send_ice_candidate_message(GstElement *webrtc G_GNUC_UNUSED, guint mlineindex, gchar *candidate,
                           gpointer user_data G_GNUC_UNUSED);
static void
on_new_transceiver(GstElement *object, GstWebRTCRTPTransceiver *candidate, gpointer udata);
static void
on_ice_gathering_state_notify(GstElement *webrtcbin, GParamSpec *pspec, gpointer user_data);
static void
on_ice_connection_state_notify(GstElement *webrtcbin, GParamSpec *pspec, gpointer user_data);
static void
on_connection_state_notify(GstElement *webrtcbin, GParamSpec *pspec, gpointer user_data);
static void on_offer_set(GstPromise *promise, gpointer user_data);
static void on_answer_created(GstPromise *promise, gpointer user_data);
static void set_offer(GstElement *webrtc, const char *sdp_offer);
static gboolean bus_call(GstBus *bus, GstMessage *msg, gpointer data);

static gchar *createPipeline(const char *sdp_offer);

int main() {
    gst_init(nullptr, nullptr);
    std::string host = "localhost";
    int port = 8080;

    httplib::Server svr;

    svr.Options(R"(\*)", [](const httplib::Request &req, httplib::Response &res) {
        res.set_header("Allow", "GET, POST, HEAD, OPTIONS");
    });
    svr.Options("/offer", [](const httplib::Request &req, httplib::Response &res) {
        res.set_header("Access-Control-Allow-Origin", "*");
        res.set_header("Allow", "GET, POST, HEAD, OPTIONS");
        res.set_header("Access-Control-Allow-Headers",
                       "X-Requested-With, Content-Type, Accept, Origin, Authorization");
        res.set_header("Access-Control-Allow-Methods", "OPTIONS, GET, POST, HEAD");
    });

    svr.Get("/hi", [](const httplib::Request &req, httplib::Response &res) {
        res.set_content("Hello World!", "text/plain");
    });

    svr.Post("/offer", [](const httplib::Request &req, httplib::Response &res) {
        res.set_header("Access-Control-Allow-Origin", "*");
        Json::Value body;
        Json::Reader reader;

        if (reader.parse(req.body, body)) {
            if (body["sdp"]) {
                const char *sdp_offer = body["sdp"].asCString();
                printf("Receive offer: \n%s\n", sdp_offer);
                gchar *answer_sdp_text = createPipeline(sdp_offer);
                // printf("Finish create pipeline\n");

               Json::Value sdp_json_answer;
               sdp_json_answer["type"] = "answer";
               sdp_json_answer["sdp"] = answer_sdp_text;
            //    const char *json_response = sdp_json_answer.asCString();
                // char json_response[1000];
                // sprintf(json_response, "{\"text\":\"answer\",\"sdp\":\"%s\"}", answer_sdp_text);
                // printf("Return SDP answer to client: %s.\n", json_response);
                // size_t response_length = strlen(json_response);
//                res.set_header("Content-type", "application/json");
                res.set_content(sdp_json_answer.toStyledString(), "application/json");
//            sdp_json_answer = cJSON_CreateObject();
//            sdp_json_answer_type = cJSON_CreateString("answer");
//            cJSON_AddItemToObject(sdp_json_answer, "type", sdp_json_answer_type);
//            sdp_json_answer_sdp = cJSON_CreateString(answer_sdp_text);
//            cJSON_AddItemToObject(sdp_json_answer, "sdp", sdp_json_answer_sdp);
//            json_response = cJSON_Print(sdp_json_answer);
//
//            printf("Return SDP answer to client: %s.\n", json_response);
//
//            response_length = strlen(json_response);
//            evhttp_add_header(req->output_headers, "Content-type", "application/json");
//            evbuffer_add(resp_buffer, json_response, response_length);
//            evhttp_send_reply(req, 200, "OK", resp_buffer);
            }
        }
    });

    svr.listen(host, port);

    return 0;
}

gchar *createPipeline(const char *sdp_offer) {
    const GstStructure *answer_reply;
    GstWebRTCSessionDescription *answer;
    GError *error = nullptr;
    gchar *answer_sdp_text = "";
    GstElement *pipeline = gst_parse_launch("videotestsrc ! video/x-raw,framerate=30/1,width=1280,height=720 ! clockoverlay ! queue ! "
                                            "vp8enc end-usage=cbr target-bitrate=0 ! rtpvp8pay ! queue ! "
                                            "application/x-rtp,media=video,payload=96,encoding-name=VP8 ! "
                                            "webrtcbin name=webrtcbin", NULL
    );
    GstElement *webrtcbin = gst_bin_get_by_name(GST_BIN (pipeline), "webrtcbin");

    g_signal_connect (webrtcbin, "on-negotiation-needed", G_CALLBACK(on_negotiation_needed),
                      nullptr);
    g_signal_connect (webrtcbin, "on-ice-candidate", G_CALLBACK(send_ice_candidate_message),
                      nullptr);
    g_signal_connect (webrtcbin, "on-new-transceiver", G_CALLBACK(on_new_transceiver), nullptr);
    g_signal_connect (webrtcbin, "notify::on-new-transceiver", G_CALLBACK(on_new_transceiver),
                      nullptr);
    g_signal_connect (webrtcbin, "notify::ice-gathering-state",
                      G_CALLBACK(on_ice_gathering_state_notify), nullptr);
    g_signal_connect (webrtcbin, "notify::ice-connection-state",
                      G_CALLBACK(on_ice_connection_state_notify), nullptr);
    g_signal_connect (webrtcbin, "notify::connection-state", G_CALLBACK(on_connection_state_notify),
                      nullptr);

    gst_element_set_state(pipeline, GST_STATE_PLAYING);

    GstBus *bus = gst_element_get_bus(pipeline);
    gst_bus_add_watch(bus, bus_call, nullptr);
    gst_object_unref(bus);

    set_offer(webrtcbin, sdp_offer);

    GstPromise *create_answer_promise = gst_promise_new();
    g_signal_emit_by_name(webrtcbin, "create-answer", NULL, create_answer_promise);

    if (gst_promise_wait(create_answer_promise) == GST_PROMISE_RESULT_REPLIED) {
        printf("create-answer promise wait over.\n");
        answer_reply = gst_promise_get_reply(create_answer_promise);
        gst_structure_get(answer_reply, "answer", GST_TYPE_WEBRTC_SESSION_DESCRIPTION, &answer,
                          NULL);
        gst_promise_unref(create_answer_promise);

        if (answer != NULL) {
            answer_sdp_text = gst_sdp_message_as_text(answer->sdp);
            gst_webrtc_session_description_free(answer);
        }
    }
    return answer_sdp_text;
}

static void set_offer(GstElement *webrtc, const gchar *sdp_offer) {
    GstWebRTCSessionDescription *offer = NULL;
    GstPromise *promise;
    GstSDPMessage *sdp;

    printf("set_offer.\n");

    gst_sdp_message_new(&sdp);

    gst_sdp_message_parse_buffer(reinterpret_cast<const guint8 *>(sdp_offer),
                                 (guint) strlen(sdp_offer), sdp);

    offer = gst_webrtc_session_description_new(GST_WEBRTC_SDP_TYPE_OFFER, sdp);

    /* Set remote description on our pipeline */
    promise = gst_promise_new_with_change_func(on_offer_set, webrtc, NULL);
    g_signal_emit_by_name(webrtc, "set-remote-description", offer, promise);
}

static void on_offer_set(GstPromise *promise, gpointer webrtc) {
    const GstStructure *reply;
    const GstPromise *answer_promise;

    printf("on_offer_set.\n");

    reply = gst_promise_get_reply(promise);
    gst_promise_unref(promise);

    answer_promise = gst_promise_new_with_change_func(on_answer_created, webrtc, NULL);
    g_signal_emit_by_name(webrtc, "create-answer", NULL, answer_promise);
}

/* Answer created by our pipeline, to be sent to the peer */
static void on_answer_created(GstPromise *promise, gpointer webrtc) {
    GstWebRTCSessionDescription *answer;
    const GstStructure *reply;
    GstPromise *set_local_promise;

    printf("on_answer_created.\n");

    g_assert_cmphex (gst_promise_wait(promise), ==, GST_PROMISE_RESULT_REPLIED);
    reply = gst_promise_get_reply(promise);
    gst_structure_get(reply, "answer", GST_TYPE_WEBRTC_SESSION_DESCRIPTION, &answer, NULL);
    gst_promise_unref(promise);

    set_local_promise = gst_promise_new();
    g_signal_emit_by_name(webrtc, "set-local-description", answer, set_local_promise);
    gst_promise_interrupt(set_local_promise);
    gst_promise_unref(set_local_promise);

    gst_webrtc_session_description_free(answer);
}

static void on_negotiation_needed(GstElement *element, gpointer user_data) {
    gchar *name;
    GstWebRTCSignalingState signalingState = static_cast<GstWebRTCSignalingState>(0);
    GstWebRTCSessionDescription *remoteDescription = NULL;

    printf("on_negotiation_needed\n");

    g_object_get(G_OBJECT (element), "name", &name, NULL);
    g_print("The name of the element is '%s'.\n", name);
    g_free(name);

    g_object_get(G_OBJECT (element), "signaling-state", &signalingState, NULL);
    g_print("The signaling state of the element is '%d'.\n", signalingState);

    g_object_get(G_OBJECT (element), "remote-description", &remoteDescription, NULL);
    if (remoteDescription == NULL) {
        g_print("Remote description is not set.\n");
    } else {
        g_print("Remote description is set.\n");
    }
}

static void send_ice_candidate_message(GstElement *webrtc G_GNUC_UNUSED, guint mlineindex,
                                       gchar *candidate, gpointer user_data G_GNUC_UNUSED) {
    //printf("send_ice_candidate_message\n");
}

static void
on_new_transceiver(GstElement *object, GstWebRTCRTPTransceiver *candidate, gpointer udata) {
    g_print("on_new_transceiver.\n");
}

static void
on_ice_gathering_state_notify(GstElement *webrtcbin, GParamSpec *pspec, gpointer user_data) {
    GstWebRTCICEGatheringState ice_gathering_state = static_cast<GstWebRTCICEGatheringState>(0);
    g_object_get(G_OBJECT (webrtcbin), "ice-gathering-state", &ice_gathering_state, NULL);
    g_print("on_ice_gathering_state_notify '%d'.\n", ice_gathering_state);
}

static void
on_ice_connection_state_notify(GstElement *webrtcbin, GParamSpec *pspec, gpointer user_data) {
    GstWebRTCICEConnectionState ice_connection_state = static_cast<GstWebRTCICEConnectionState>(0);
    g_object_get(G_OBJECT (webrtcbin), "ice-connection-state", &ice_connection_state, NULL);
    g_print("on_ice_connection_state_notify '%d'.\n", ice_connection_state);
}

static void
on_connection_state_notify(GstElement *webrtcbin, GParamSpec *pspec, gpointer user_data) {
    GstWebRTCPeerConnectionState connection_state = static_cast<GstWebRTCPeerConnectionState>(0);
    g_object_get(G_OBJECT (webrtcbin), "connection-state", &connection_state, NULL);
    g_print("on_connection_state_notify '%d'.\n", connection_state);

    if (connection_state == GST_WEBRTC_PEER_CONNECTION_STATE_FAILED) {
        g_print("Peer connections failed, shutting down pipeline.\n");
        gst_element_set_state(webrtcbin, GST_STATE_NULL);
        gst_object_unref(webrtcbin);
    }
}

static gboolean bus_call(GstBus *bus, GstMessage *msg, gpointer data) {
    switch (GST_MESSAGE_TYPE (msg)) {

        case GST_MESSAGE_EOS:
            g_print("End of stream\n");
            break;

        case GST_MESSAGE_ERROR: {
            gchar *debug;
            GError *error;

            gst_message_parse_error(msg, &error, &debug);
            g_free(debug);

            g_printerr("Error: %s\n", error->message);
            g_error_free(error);

            break;
        }
        default:
            break;
    }

    return TRUE;
}
